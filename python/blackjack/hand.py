from blackjack.card import Card


class Hand:
    def __init__(self):
        self.cards = []

    @property
    def value(self):
        total = 0
        for card in self.cards:
            total += card.value if card.value < 11 else (1 if total > 10 else 11)
        return total

    def hit(self, card: Card):
        self.cards.append(card)
        self.cards.sort()  # sort so value of aces is added last in value-property

    def valid(self):
        return self.value <= 21
