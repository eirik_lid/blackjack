class Card:
    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit

    def __str__(self):
        return f"{self.suit.name} {self.rank_str()}"

    def __lt__(self, other):
        return (
            self.value < other.value
            if self.value != other.value  # Let face cards be sorted by rank
            else self.rank < other.rank
        )

    def __eq__(self, other):
        return self.rank == other.rank

    def __repr__(self):
        return f"Card( rank={self.rank}, suit={self.suit})"

    def rank_str(self):
        if 1 < self.rank < 11:
            return self.rank
        else:
            conversion = {1: "A", 11: "J", 12: "Q", 13: "K"}
            return conversion[self.rank]

    @property
    def value(self):
        # Ace
        if self.rank == 1:
            return 11
        else:
            return min(self.rank, 10)
