from blackjack.deck import Deck
from blackjack.hand import Hand


def main():

    deck = Deck()
    player_hand = Hand()
    dealer_hand = Hand()

    # Dealer draws first
    card = deck.cards.pop()
    dealer_hand.hit(card)
    print(f"Dealer draws {card}. Dealers total is {dealer_hand.value}")

    while player_hand.valid():
        read = input("Stand, Hit:\n")
        if read == "Hit":
            card = deck.cards.pop()
            player_hand.hit(card)
            print(f"Hit with {card}. Total is {player_hand.value}")
        elif read == "Stand":
            break

    if not player_hand.valid():
        print("Bust! Better luck next time.")
        exit()

    while dealer_hand.value < 17:
        card = deck.cards.pop()
        dealer_hand.hit(card)
        print(f"Dealer draws {card}. Dealers total is {dealer_hand.value}")

    if not dealer_hand.valid():
        print("Dealer busts! YOU WIN!")

    if dealer_hand.value > player_hand.value:
        print(f"Dealer wins...")
    elif dealer_hand.value < player_hand.value:
        print("YOU WIN!")
    else:
        print("Push. No one wins.")


if __name__ == "__main__":
    main()
