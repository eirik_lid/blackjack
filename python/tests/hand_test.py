import pytest

from blackjack.card import Card
from blackjack.hand import Hand


@pytest.mark.parametrize(
    "cards, expected",
    [
        ([Card(rank=1, suit=1)], 11),
        ([Card(rank=1, suit=1), Card(rank=1, suit=1)], 12),
        ([Card(rank=1, suit=1), Card(rank=10, suit=1)], 21),
        ([Card(rank=12, suit=1), Card(rank=1, suit=1)], 21),
        ([Card(rank=1, suit=1), Card(rank=12, suit=1), Card(rank=13, suit=1)], 21),
        ([Card(rank=1, suit=1), Card(rank=12, suit=1), Card(rank=5, suit=1)], 16),
    ],
)
def test_ace_values(cards, expected):
    hand = Hand()
    for card in cards:
        hand.hit(card)

    assert hand.value == expected


@pytest.mark.parametrize(
    "cards, expected",
    [
        ([Card(rank=1, suit=1)], True),
        ([Card(rank=1, suit=1), Card(rank=1, suit=1)], True),
        ([Card(rank=1, suit=1), Card(rank=10, suit=1)], True),
        ([Card(rank=12, suit=1), Card(rank=1, suit=1)], True),
        ([Card(rank=1, suit=1), Card(rank=12, suit=1), Card(rank=13, suit=1)], True),
        ([Card(rank=1, suit=1), Card(rank=12, suit=1), Card(rank=5, suit=1)], True),
        ([Card(rank=11, suit=1), Card(rank=12, suit=1), Card(rank=13, suit=1)], False),
        ([Card(rank=10, suit=1), Card(rank=8, suit=1), Card(rank=4, suit=1)], False),
    ],
)
def test_hand_valid(cards, expected):
    hand = Hand()
    for card in cards:
        hand.hit(card)

    assert hand.valid() == expected
