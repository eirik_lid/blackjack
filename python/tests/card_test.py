import pytest

from blackjack.card import Card


@pytest.mark.parametrize(
    "cards, expected",
    [
        (
            [Card(rank=1, suit=1), Card(rank=2, suit=1)],
            [Card(rank=2, suit=1), Card(rank=1, suit=1)],
        ),
        (
            [Card(rank=1, suit=1), Card(rank=10, suit=1)],
            [Card(rank=10, suit=1), Card(rank=1, suit=1)],
        ),
        (
            [Card(rank=12, suit=1), Card(rank=1, suit=1)],
            [Card(rank=12, suit=1), Card(rank=1, suit=1)],
        ),
        (
            [Card(rank=1, suit=1), Card(rank=7, suit=1), Card(rank=13, suit=1)],
            [Card(rank=7, suit=1), Card(rank=13, suit=1), Card(rank=1, suit=1)],
        ),
        (
            [Card(rank=1, suit=1), Card(rank=12, suit=1), Card(rank=5, suit=1)],
            [Card(rank=5, suit=1), Card(rank=12, suit=1), Card(rank=1, suit=1)],
        ),
        (
            [Card(rank=13, suit=1), Card(rank=11, suit=1), Card(rank=12, suit=1)],
            [Card(rank=11, suit=1), Card(rank=12, suit=1), Card(rank=13, suit=1)],
        ),
        (
            [
                Card(rank=5, suit=1),
                Card(rank=4, suit=1),
                Card(rank=2, suit=1),
                Card(rank=7, suit=1),
                Card(rank=6, suit=1),
                Card(rank=3, suit=1),
            ],
            [
                Card(rank=2, suit=1),
                Card(rank=3, suit=1),
                Card(rank=4, suit=1),
                Card(rank=5, suit=1),
                Card(rank=6, suit=1),
                Card(rank=7, suit=1),
            ],
        ),
    ],
)
def test_card_sort(cards, expected):
    cards.sort()
    assert cards == expected
