import pytest

from blackjack.deck import Deck


@pytest.fixture
def deck():
    return Deck()


def test_should_have_52_cards(deck):
    assert 52 == len(deck.cards)


def test_should_have_4_distinct_suits(deck):
    assert 4 == len(set([card.suit for card in deck.cards]))
